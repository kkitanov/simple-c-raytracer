﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace Raytracer
{
    public partial class Form1 : Form
    {
        RaytracerLib.Raytracer tracer = new RaytracerLib.Raytracer();
        int startTicks = 0;
        System.Drawing.Bitmap bitmap;

        public Form1()
        {
            InitializeComponent();

            SamplesListBox.SelectedIndex = 0;
            SecondaryRaysListBox.SelectedIndex = 2;
            shadowSamplesList.SelectedIndex = 4;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tracer.Raycastables.Clear();
            tracer.Lights.Clear();
            bitmap = new System.Drawing.Bitmap(pictureBox1.Width, pictureBox1.Height);

            RaytracerLib.Plane plane = new RaytracerLib.Plane(new Vector3D(0, -1, 0), new Vector3D(0, 1, 0));
            plane.Reflectivity = 1;
            tracer.Raycastables.Add(plane);

            RaytracerLib.Sphere sphere = new RaytracerLib.Sphere(new Vector3D(-1, 0, -3), 0.5);
            sphere.Color = new Vector3D(0.6, 0, 0.1);
            sphere.Reflectivity = 1;
            tracer.Raycastables.Add(sphere);

            RaytracerLib.Sphere sphere1 = new RaytracerLib.Sphere(new Vector3D(1, 0, -3), 0.5);
            sphere1.Color = new Vector3D(0, 0, 0.3f);
            sphere1.Refractivity = 0.5f;
            sphere1.RefractionIndex = 1.1 ;
            tracer.Raycastables.Add(sphere1);

            for (int i = 0; i < 20; i++)
            {
                RaytracerLib.Sphere sphere2 = new RaytracerLib.Sphere(new Vector3D(i-10, 0, 5), 0.5);
                sphere2.Color = new Vector3D(0.2, 1, 0.5);
                tracer.Raycastables.Add(sphere2);
            }

            RaytracerLib.Light light = new RaytracerLib.Light(new Vector3D(0, 0, -4));
            tracer.Lights.Add(light);

            RaytracerLib.Light light1 = new RaytracerLib.Light(new Vector3D(0, 0, -2));
            tracer.Lights.Add(light1);

            startTicks = Environment.TickCount;

            tracer.Raytrace(pictureBox1.Width,
                pictureBox1.Height,
                SamplesListBox.SelectedIndex + 1,
                SecondaryRaysListBox.SelectedIndex,
                shadowSamplesList.SelectedIndex,
                (a, b) =>
                {
                    progressBar.Value = b.ProgressPercentage;
                    RaytracerLib.Raytracer.StateUpdate state = b.UserState as RaytracerLib.Raytracer.StateUpdate;

                    for (int y = 0; y < pictureBox1.Height; y++)
                    {
                        Vector3D color = state.colors[state.x, y];
                        bitmap.SetPixel(state.x, y, System.Drawing.Color.FromArgb((int)(color.X * 255), (int)(color.Y * 255), (int)(color.Z * 255)));
                    }

                    if (progressBar.Value % 10 == 0)
                    {
                        pictureBox1.Image = bitmap;
                    }

                },
                (a, b) =>
                {
                    statusLabel.Text = "Rendering completed in " + ((Environment.TickCount - startTicks) / 1000.0f).ToString() + " seconds";
                    // pictureBox1.Image = b.Result as Bitmap;
                }
            );
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            tracer.CameraPosition.X = (double)numericUpDown1.Value;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            tracer.CameraPosition.Y = (double)numericUpDown2.Value;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            tracer.CameraPosition.Z = (double)numericUpDown3.Value;
        }
    }
}
