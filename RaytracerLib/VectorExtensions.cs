﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace RaytracerLib
{
    public static class VectorExtensions
    {
        public static Vector3D Multiply(this Vector3D a, Vector3D b)
        {
            return new Vector3D(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
        }

        public static Vector3D Clamp(this Vector3D a, double min, double max)
        {
            double x = Math.Min(Math.Max(min, a.X), max);
            double y = Math.Min(Math.Max(min, a.Y), max);
            double z = Math.Min(Math.Max(min, a.Z), max);

            return new Vector3D(x, y, z);
        }

        public static bool IsZero(this Vector3D a)
        {
             if(Math.Abs(a.X) < 0.001)
            {
                if (Math.Abs(a.Y) < 0.001)
                {
                    if (Math.Abs(a.Z) < 0.001)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // Look at http://www.flipcode.com/archives/reflection_transmission.pdf for details on reflection
        public static Vector3D Reflected(this Vector3D a, Vector3D Normal)
        {
            return a - 2 * Normal * (Vector3D.DotProduct(a, Normal));
        }

        // Look at http://www.flipcode.com/archives/reflection_transmission.pdf for details on refraction
        public static Vector3D Refracted(this Vector3D a, Vector3D Normal, double ri1, double ri2)
        {
            double refraction = ri1 / ri2;
            double cos1 = -Vector3D.DotProduct(Normal, a);
            double cos2 = 1.0f - refraction * refraction * (1.0 - cos1 * cos1);
            if (cos2 > 0)
            {
                return (refraction * a) + (refraction*cos1 - Math.Sqrt(cos2)) * Normal;
            }

            return new Vector3D(0, 0, 0);
        }
    }
}
