﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace RaytracerLib
{
    public class Light
    {
        public Light(Vector3D Position)
        {
            this.Position = Position;
        }
        public Vector3D Position;
        public Vector3D Color = new Vector3D(1, 1, 1);

    }
}
