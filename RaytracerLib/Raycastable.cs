﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace RaytracerLib
{
    public abstract class Raycastable
    {
        // Returns whether the ray will intersect the Raycastable object and the point at which the collision happens
        public abstract bool Raycast(Vector3D origin, Vector3D dir, out double t, out bool inside);
        // Returns the normal vector at the point (world coordinates)
        public abstract Vector3D GetNormal(Vector3D point);

        public Vector3D Color = new Vector3D(1, 1, 1);
        // Between [0,1]
        public double Reflectivity = 0;
        public double Refractivity = 0;
        public double RefractionIndex = 1;
    }
}
