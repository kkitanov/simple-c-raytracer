﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Media3D;
using System.ComponentModel;

namespace RaytracerLib
{
    public class Raytracer
    {
        public class StateUpdate
        {
            public int x;
            public Vector3D[,] colors;
        };

        public List<Raycastable> Raycastables = new List<Raycastable>();
        public List<Light> Lights = new List<Light>();

        public Raycastable[] RaycastablesArray;

        public void Raytrace(int width, int height, int antiAliasingSamples, int maxSecondaryDepth, int shadowSamples, ProgressChangedEventHandler progressFunc, RunWorkerCompletedEventHandler completedFunc)
        {
            RaycastablesArray = Raycastables.ToArray();

            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = true;

            worker.ProgressChanged += progressFunc;
            worker.RunWorkerCompleted += completedFunc;
            worker.DoWork += (obj, args) => 
            {
                int workDone = 0;
                Vector3D[,] colors = new Vector3D[width, height];

                // Execute the outer for in parallel - this improves performance on multi-core machines
                Parallel.For(0, width, x =>
                {
                    for (int y = 0; y < height; y++)
                    {
                        colors[x, y] = RaytracePixel(x, y, width, height, antiAliasingSamples, maxSecondaryDepth, shadowSamples);
                    }

                    lock (worker)
                    {
                        workDone += height;

                        StateUpdate su = new StateUpdate();
                        su.colors = colors;
                        su.x = x;

                        worker.ReportProgress( (100*workDone) / (width * height), su);
                    }

                });

                args.Result = colors;
            };

            worker.RunWorkerAsync();

        }

        private Vector3D RaytracePixel(int x, int y, int width, int height, int antiAliasingSamples, int maxSecondaryDepth, int shadowSamples)
        {
            // Background color is black
            Vector3D result = new Vector3D(0, 0, 0);

            // We need to get the coordinates of the near plane from the pixel positions
            // View angle is determined by distance to the near plane and screen resoltuion

            double dx = (double)(1.0) / (double)(width);
            double dy = (double)(1.0) / (double)(height);
            double aspect = (double)(width) / (double)(height);

            for (int i = 0; i < antiAliasingSamples; i++)
            {
                for (int j = 0; j < antiAliasingSamples; j++)
                {
                    double viewPlaneX = (double)(x) / (double)(width) + dx * (double)(i) / (double)(antiAliasingSamples);
                    double viewPlaneY = (double)(y) / (double)(height) + dy * (double)(j) / (double)(antiAliasingSamples);

                    // The view rect has a size of 1 unit, centered at 0,0
                    viewPlaneX = (viewPlaneX - 0.5) * aspect;
                    // Invert the Y axis as in image space positive is down and negative is up. In all other spaces its the reverse.
                    viewPlaneY = -(viewPlaneY - 0.5);


                    Vector3D origin = CameraPosition;
                    Vector3D dir = viewPlaneX * CameraRight + viewPlaneY * CameraUp + nearPlaneDist * CameraDirection;
                    dir.Normalize();

                    result += Shade(origin, dir, 1, 0, maxSecondaryDepth, shadowSamples);
                }
            }


            return (result / (antiAliasingSamples * antiAliasingSamples)).Clamp(0, 1);
        }

        private Vector3D Shade(Vector3D origin, Vector3D dir, double incomingRefractionIndex, int depth, int maxSecondaryDepth, int shadowSamples)
        {
            Vector3D result = new Vector3D(0, 0, 0);
            // Primary ray
            double closestDist;
            Raycastable closest = null;
            bool inside;
            ShootRay(origin, dir, farPlaneDist, false, out closestDist, out inside, out closest);

            if (closest != null)
            {
                Vector3D intersecPos = origin + closestDist * dir;
                Vector3D normal = closest.GetNormal(intersecPos) ;

                Vector3D objColor = closest.Color;

                // Secondary rays for reflection/refraction
                if (depth + 1 <= maxSecondaryDepth)
                {
                    if (closest.Reflectivity > 0)
                    {
                        Vector3D reflected = dir.Reflected(normal);
                        Vector3D reflectionColor = Shade(intersecPos + reflected * 0.001, reflected, incomingRefractionIndex, depth + 1, maxSecondaryDepth, shadowSamples);
                        result += objColor.Multiply(reflectionColor * closest.Reflectivity);
                    }

                    if (closest.Refractivity > 0)
                    {
                        Vector3D refracted = dir.Refracted(normal * (inside ? -1 : 1), incomingRefractionIndex, closest.RefractionIndex);
                        if (refracted.IsZero() == false)
                        {
                            Vector3D refractionColor = Shade(intersecPos + refracted * 0.001, refracted, closest.RefractionIndex, depth + 1, maxSecondaryDepth, shadowSamples);
                            result += objColor.Multiply(refractionColor);
                        }

                    }
                }


                Random r = new Random();
                foreach (Light light in Lights)
                {

                    double shadowTerm = 1;
                    // If shadow samples == 0, we are not going to render any shadows
                    // If shadow samples = 1, we are going to do hard shadows
                    if (shadowSamples > 0)
                    {
                        int shadowSamplesHit = 0;
                        int totalShadowSamples = shadowSamples * shadowSamples;
                        for (int i = 0; i < totalShadowSamples; i++)
                        {
                            double rol = i / shadowSamples;
                            double col = i % shadowSamples;

                            // This replaces banding with noise
                            if ( totalShadowSamples > 1)
                            {
                                rol += (double)r.NextDouble();
                                col += (double)r.NextDouble();
                            }
                            double x = (rol / shadowSamples) * 2.0 - 1.0 ;
                            double z = (col / shadowSamples) * 2.0 - 1.0 ;

                            Vector3D objToLight = light.Position - intersecPos + new Vector3D(x, 0, z) * 0.2;
                            double objToLightLen = objToLight.Length;
                            Vector3D objToLightNorm = objToLight / objToLightLen;
                            Vector3D offsetPosTowardsLight = intersecPos + objToLightNorm * 0.001;

                            // Shadow ray
                            Raycastable occluder = null;

                            ShootRay(offsetPosTowardsLight, objToLightNorm, objToLightLen, true, out closestDist, out inside, out occluder);

                            if (occluder == null)
                            {
                                shadowSamplesHit++;
                            }
                        }
                        shadowTerm = (double)shadowSamplesHit / totalShadowSamples;
                    }

                    Vector3D L = light.Position - intersecPos;
                    L.Normalize();

                    double diffuseTerm = Vector3D.DotProduct(L, normal);
                    if (diffuseTerm > 0)
                    {
                        result += objColor.Multiply(light.Color) * diffuseTerm * shadowTerm;
                    }

                }
            }

            return result;
        }

        // Shoots a ray into the scene and returns the closest object that collides with (if any).
        // Alternatively. if its a shadowRay it will return the first object that collides with the light
        private void ShootRay(Vector3D origin, Vector3D dir, double maxDist, bool shadowRay, out double closestDist, out bool inside, out Raycastable closest)
        {
            closestDist = maxDist;
            closest = null;
            inside = false;

            double t;
            bool tempInside;
            Raycastable obj;
            for (int i = 0; i < RaycastablesArray.Length; i++)
            {
                obj = RaycastablesArray[i];
                if (obj.Raycast(origin, dir, out t, out tempInside))
                {
                    if (t < closestDist)
                    {
                        closestDist = t;
                        closest = obj;
                        inside = tempInside;

                        if (shadowRay)
                        {
                            return;
                        }
                    }

                }
            }
        }
        public double nearPlaneDist = 1, farPlaneDist = 1000;
        public Vector3D CameraPosition = new Vector3D(0, 0, -5), CameraRight = new Vector3D(1, 0, 0), CameraUp = new Vector3D(0, 1, 0), CameraDirection = new Vector3D(0, 0, 1);
    }
}
