﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace RaytracerLib
{
    public class Sphere : Raycastable
    {
        public Vector3D Center;
        public double Radius;

        public Sphere(Vector3D Center, Double Radius)
        {
            this.Radius = Radius;
            this.Center = Center;
        }

        // Returns whether the ray will intersect the Raycastable object and the point at which the collision happens
        public override bool Raycast(Vector3D origin, Vector3D dir, out double t, out bool inside)
        {
            bool intersect = false;
            inside = false;
            t = 0;

            // Sphere equation |(point - center)| = radius
            // Substitue point for the ray equation : origin + t*dir = point
            // Solve for t

            double radiSquare = Radius * Radius;
            Vector3D fromCenterToOrigin = origin - Center;
            double dotProd = Vector3D.DotProduct(fromCenterToOrigin, dir);
            var discriminant = dotProd * dotProd - Vector3D.DotProduct(fromCenterToOrigin, fromCenterToOrigin) + radiSquare;

            if (discriminant >= 0)
            {
                double root = (double) Math.Sqrt(discriminant);
                t = -dotProd - root;

                if (t < 0)
                {
                    t = -dotProd + root;
                    inside = true;
                }
            }

            if (t > 0)
            {
                intersect = true;
            }

            return intersect;

        }
        // Returns the normal vector at the point (world coordinates)
        public override Vector3D GetNormal(Vector3D point)
        {
            Vector3D result = (point - Center);
            result.Normalize();

            return result;
        }
    }
}
