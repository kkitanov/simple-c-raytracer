﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace RaytracerLib
{
    public class Plane : Raycastable
    {
        public Vector3D Position, Normal;

        public Plane(Vector3D Position, Vector3D  Normal)
        {
            this.Normal = Normal;
            this.Position = Position;
            this.Normal.Normalize();
        }

        // Returns whether the ray will intersect the Raycastable object and the point at which the collision happens
        public override bool Raycast(Vector3D origin, Vector3D dir, out double t, out bool inside)
        {
            bool intersect = false;
            inside = false;
            t = 0;

            // Plane equation  (Position - p). normal = 0
            // Substitue p for the ray equation : origin + t*dir = p
            // Solve for t
            double dotProduct = Vector3D.DotProduct(dir, Normal);

            if (dotProduct == 0)
            {
                intersect = false;
            }
            else
            {
                t = Vector3D.DotProduct(Normal, Position - origin) / dotProduct;

                intersect = (t >= 0);
            }
 
            return intersect;

        }
        // Returns the normal vector at the point (world coordinates)
        public override Vector3D GetNormal(Vector3D point)
        {
            return Normal;
        }

    }
}
