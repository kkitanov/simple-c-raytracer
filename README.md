#Simple Raytracing library

## Intoduction

This library is able to render images of virtual 3D scenes using a technique known as ray tracing.
It has the ability to render (soft) shadows, simple diffuse lighting with multiple lights, reflections, refractions and anti-aliasing.

The ray tracer supports multithreading via a Parallel.For loop. There is second branch (simd) that implements Microsoft's Numberics.Vectors library which increases performance by around 40% on modern CPUs. Rendering is done in a progressive way, that lets the user see the intermediate results of the raytraced image.

Supported primitives: Spheres and Planes.

The ray tracer comes with an example program that shows how to use it.

##Example usage:

            RaytracerLib.Sphere sphere = new RaytracerLib.Sphere(new Vector3D(-1, 0, -3), 0.5);
            sphere.Color = new Vector3D(0.6, 0, 0.1);
            sphere.Reflectivity = 1;
            tracer.Raycastables.Add(sphere);

            RaytracerLib.Light light = new RaytracerLib.Light(new Vector3D(0, 0, -4));
            tracer.Lights.Add(light);

            pictureBox1.Image = tracer.Raytrace(pictureBox1.Width, pictureBox1.Height, SamplesListBox.SelectedIndex + 1,   SecondaryRaysListBox.SelectedIndex, ShadowsCheckbox.Checked);